document.addEventListener("DOMContentLoaded", function() {
    var citySelect = document.getElementById("city");
    var districtSelect = document.getElementById("district");
    
    var districts = {
        'Hà Nội': ["Chọn quận","Hoàng Mai", "Thanh Trì", "Nam Từ Liêm", "Hà Đông", "Cầu Giấy"],
        'TP.Hồ Chí Minh': ["Chọn quận","Quận 1", "Quận 2", "Quận 3", "Quận 7", "Quận 9"]
    };

    citySelect.addEventListener("change", function() {
        var selectedCity = citySelect.value;
        districtSelect.innerHTML = "";
        
        for (var i = 0; i < districts[selectedCity].length; i++) {
            var option = document.createElement("option");
            option.value = districts[selectedCity][i];
            option.text = districts[selectedCity][i];
            districtSelect.appendChild(option);
        }
    });
});
$(document).ready(function() {
    $("#registrationForm").submit(function(event) {
        var usernameValid = true;
        var genderValid = true;
        var birthdayValid = true;
        var addressValid = true;

        // Kiểm tra tên
        var username = $("#username").val();
        if (username === "") {
            $("#usernameMessage").text("Hãy nhập tên");
            usernameValid = false;
        } else {
            $("#usernameMessage").text(""); 
        }

        // Kiểm tra giới tính
        var gender = $("input[name='gender[]']:checked").length;
        if (gender === 0) {
            $("#genderMessage").text("Hãy chọn giới tính");
            genderValid = false;
        } else {
            $("#genderMessage").text("");
        }

        // Kiểm tra ngày, tháng, năm sinh
        var day = $("#day").val();
        var month = $("#month").val();
        var year = $("#year").val();
        if (day === "" || month === "" || year === "") {
            $("#birthdayMessage").text("Hãy chọn ngày sinh");
            birthdayValid = false;
        } else {
            $("#birthdayMessage").text("");
        }

        // Kiểm tra địa chỉ (thành phố và quận)
        var city = $("#city").val();
        var district = $("#district").val();
        if (city == "" || district == "Chọn quận") {
            $("#addressMessage").text("Hãy chọn địa chỉ");
            addressValid = false;
        } else {
            $("#addressMessage").text(""); // Xóa thông báo khi không có lỗi
        }

        // Ngăn chặn việc submit form nếu bất kỳ trường nào không hợp lệ
        if (!usernameValid || !genderValid || !birthdayValid || !addressValid) {
            event.preventDefault();
        }
    });
});