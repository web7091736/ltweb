<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="register.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="students.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
</head>

<body>
    <div class="">
        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $username = $_POST["username"];
            $gender = $_POST["gender"];
            $day = $_POST["day"];
            $month = $_POST["month"];
            $year = $_POST["year"];
            $city = $_POST["city"];
            $district = $_POST["district"];
            $otherInfo = $_POST["otherInfo"];

            echo "
            <table class='table table-sm' style='margin-left:50px'>
            <div class='input-container'>
            <tr>
                <th scope='row' class='table-secondary'>
            <div class='user-name' style='display: inline-block; '>
                <label for=user-name'><span class='text'>Họ và tên</span></label>
            </div>
            </th>
            <td>
            <div class='output-text' style='display: inline-block; '>
            $username
            </div>
            </td>
            </tr>
        </div>  ";

            if (is_array($gender)) {
                if (implode(", ", $gender) == '0') {
                    echo "<div class='input-container'>
                    <tr>
                    <th scope='row'  class='table-secondary'>
                    <div class='user-name' style='display: inline-block; '>
                    <label><span class='text'>Giới tính</span></label>
                    </div>
                    </th>
                    <td>
                    <div class='output-text' style='display: inline-block; '>Nam</div>
                    </td>
                    </tr>
                </div>";
                }
                if (implode(", ", $gender) == '1') {
                    echo "<div class='input-container'>
                    <tr>
                    <th scope='row' class='table-secondary'>
                    <div class='user-name' style='display: inline-block; '>
                    <label><span class='text'>Giới tính</span></label>
                    </div>
                    </th>
                    <td>
                    <div class='output-text' style='display: inline-block; '>Nữ </div>
                    </td>
                    </tr>
                </div>";
                }
            }
            echo "            <div class='input-container'>
            <tr>
            <th scope='row' class='table-secondary'>
                <div class='user-name' style='display: inline-block; '>
                    <label><span class='text'>Ngày sinh</span></label>
                </div>
            </th>
            <td>
    <div class='output-text' style='display: inline-block; '>$day/$month/$year</div>
    </div>
    </td>
    </tr>";

            echo "<div class='input-container'>
            <tr>
            <th scope='row' class='table-secondary'>
        <div class='user-name' style='display: inline-block; '>
            <label>Địa chỉ</label>
            </th>
            <td>
            <div class='output-text' style='display: inline-block; '>$district - $city</div>
        </div>
        </td>
        </tr>
    </div>";

            echo "<div class='input-container'>
            <tr>
            <th scope='row' class='table-secondary'>
        <div class='user-name' style='display: inline-block;margin-top:30px; '>
            <label>Thông tin khác</label>
            </th>
            <td>
            <div class='output-text' style='display: inline-block;height:100px;'>$otherInfo</div>
        </div>
        </td>
        </tr>
    </div>";
            echo "</p>";
        }
        ?>
        </table>
    </div>
</body>

</html>
<style>
    th{
        width: 20%;
    }
    .output-text{
        margin-left: 40px;
    }
</style>