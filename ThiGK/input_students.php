<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="register.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="students.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
</head>

<body>
    <h1 class="text-center"><strong>Form đăng ký sinh viên</strong></h1>
    <div class="container">
        <p class='error' id="usernameMessage" style="color: red;"></p>
        <p class='error' id="genderMessage" style="color: red;"></p>
        <p class='error' id="addressMessage" style="color: red;"></p>
        <p class='error' id="birthdayMessage" style="color: red;"></p>
        <form method="POST" id="registrationForm" action="regist_student.php" enctype="multipart/form-data" name="submitForm">
            <table class="table" style="margin-left:5   0px;">
                <div class="input-container">
                    <tr>
                        <th scope="row" class='table-secondary'>
                            <div class="user-name" style="display: inline-block; vertical-align: middle;">
                                <label for="username"><span class="text">Họ và tên</span></label>
                            </div>
                        </th>
                        <td class="text-center">
                            <div class="input-text">
                                <input type="text" id="username" name="username" style="width: 100%;">
                            </div>
                        </td>

                    </tr>
                </div>

                <div class="input-container">
                    <tr>
                        <th scope="col" class='table-secondary'>
                            <div class="user-name" style="display: inline-block; vertical-align: middle;">
                                <label><span class="text">Giới tính</span> </label>
                            </div>
                        </th>
                        <td>
                            <div class="genderInput" style="display: inline-block; vertical-align: middle;">
                                <?php
                                $genders = array(0 => 'Nam', 1 => 'Nữ'); // Danh sách giới tính
                                for ($key = 0; $key < count($genders); $key++) {
                                    echo '<label class="custom-checkbox">';
                                    echo '<input class="" type="radio" id="gender' . $key . '" name="gender[]" value="' . $key . '" >';
                                    echo '<span class="checkmark"></span>'; // Thêm hình tròn
                                    echo $genders[$key];
                                    echo '</label>';
                                }
                                ?>
                            </div>
                        </td>
                    </tr>
                </div>

                <div class="input-container">
                    <tr>
                        <th scope="row" class='table-secondary'>
                            <div class="user-name" style="display: inline-block; vertical-align: middle;">
                                <label><span class="text">Ngày sinh</span> </label>
                            </div>
                        </th>
                        <td>
                            <div style="display: inline-block; vertical-align: middle;">
                                <select name="day" id="day" class="input">
                                    <option value="">Ngày</option>
                                    <?php
                                    for ($day = 1; $day <= 31; $day++) {
                                        echo "<option value='{$day}'>{$day}</option>";
                                    }
                                    ?>
                                </select>

                                <select name="month" id="month" class="input">
                                    <option value="">Tháng</option>
                                    <?php
                                    for ($month = 1; $month <= 12; $month++) {
                                        echo "<option value='{$month}'>{$month}</option>";
                                    }
                                    ?>
                                </select>

                                <select name="year" id="year" class="input">
                                    <option value="">Năm</option>
                                    <?php
                                    $currentYear = date('Y');
                                    $startYear = $currentYear - 40;
                                    $endYear = $currentYear - 15;
                                    for ($year = $endYear; $year >= $startYear; $year--) {
                                        echo "<option value='{$year}'>{$year}</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </td>
                    </tr>
                </div>

                <div class="input-container">
                    <tr>
                        <th scope="row" class='table-secondary'>
                            <div class="user-name" style="display: inline-block; vertical-align: middle;">
                                <label for="address"><span class="text">Địa chỉ</span> </label>
                            </div>
                        </th>
                        <td>
                            <div class="address-input" style="display: inline-block; vertical-align: middle;">
                                <select name="city" id="city" class="input">
                                    <option value="">Thành phố</option>
                                    <option value="Hà Nội">Hà Nội</option>
                                    <option value="TP.Hồ Chí Minh">TP.Hồ Chí Minh</option>
                                </select>

                                <select name="district" id="district" class="input">
                                    <option value="Chọn quận">Chọn quận</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                </div>


                <div class="input-container">
                    <tr>
                        <th scope="row" class='table-secondary'>
                            <div class="user-name" style="display: inline-block; vertical-align: middle;">
                                <label for="otherInfo"><span class="text">Thông tin khác</span></label>
                            </div>
                        </th>
                        <td>
                                <textarea name="otherInfo" class="input-textarea" rows="4" cols="50" style="width: 100%;"></textarea>
                        </td>
                    </tr>
                </div>
            </table>
            <div class="submit-container">
                <div class="text-center">
                    <div class="submit-button" style="display: inline-block;">
                        <input class="btn btn-success" type="submit" value="Đăng ký" style="vertical-align: middle;">
                    </div>
                </div>
            </div>
        </form>
    </div>

</body>

</html>
<style>
    .input-text input,.input-textarea {
        background-color: transparent; 
        border: none; 
        width: 100%; 
    }
</style>
