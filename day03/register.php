<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="register.css">
</head>
<body onload="loadPHPContent()">
    <div class="container">
        <div id="phpContent">
            <?php
            // Đoạn mã PHP ở đây để xử lý nội dung PHP (nếu cần).
            ?>
        </div>
        <form method="POST">
            <div class="input-container">
                <div class="user-name" style="display: inline-block; vertical-align: middle;">
                    <label for="username">Họ và tên</label>
                </div>
                <div class="input" style="display: inline-block; vertical-align: middle;">
                    <input type="text" id="username" name="username" class="blue-rectangle" style="background-color: white; border: none; width: 98%; height: 90%;">
                </div>
            </div>    
            <div class="input-container">
                <div class="user-name" style="display: inline-block; vertical-align: middle;">
                    <label>Giới tính</label>
                </div>
                <div class="genderInput" style="display: inline-block; vertical-align: middle;">
                    <?php
                    $genders = array(0 => 'Nam', 1 => 'Nữ'); // Danh sách giới tính
                    for ($key = 0; $key < count($genders); $key++) {
                        echo '<label class="custom-checkbox">';
                        echo '<input class="" type="radio" id="gender' . $key . '" name="gender[]" value="' . $key . '">';
                        echo '<span class="checkmark"></span>'; // Thêm hình tròn
                        echo $genders[$key];
                        echo '</label>';
                    }
                    ?>
                </div>
            </div>

            <div class="input-container">
                <div class="user-name" style="display: inline-block; vertical-align: middle;">
                    <label>Phân khoa</label>
                </div>
                <div  style=" display: inline-block; vertical-align: middle;">
                    <select class="input" name="department">
                        <option value="PK">--Chọn phân khoa--</option>
                        <?php
                        $departments = array("KHMT" => "Khoa Học Máy Tính", "KHVL" => "Khoa Học Vật Lý"); // Danh sách các phân khoa
                        foreach ($departments as $key => $department) {
                            echo '<option value="' . $key . '">' . $department . '</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>

            <div class="submit-container">
                <div class="submit-button" style="display: inline-block; vertical-align: middle;">
                    <input type="submit" value="Đăng ký" style="background-color: rgb(115,173,73);color: white; border: none;">
                </div>
            </div>
        </form>
    </div>
</body>
</html>
