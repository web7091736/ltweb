$(document).ready(function() {
    $("#registrationForm").submit(function(event) {
        // Biến để kiểm tra trạng thái của từng trường
        var usernameValid = true;
        var genderValid = true;
        var departmentValid = true;
        var birthdayValid = true;

        // Kiểm tra giá trị của trường username
        var username = $("#username").val();
        if (username === "") {
            $("#usernameMessage").text("Hãy nhập tên");
            usernameValid = false;
        } else {
            $("#usernameMessage").text(""); // Xóa thông báo khi không có lỗi
        }

        // Kiểm tra giá trị của trường gender
        var gender = $("input[name='gender[]']:checked").length;
        if (gender === 0) {
            $("#genderMessage").text("Hãy chọn giới tính");
            genderValid = false;
        } else {
            $("#genderMessage").text("");
        }

        // Kiểm tra giá trị của trường department
        var department = $("#department").val();
        if (department === "PK") {
            $("#departmentMessage").text("Hãy chọn phân khoa");
            departmentValid = false;
        } else {
            $("#departmentMessage").text("");
        }

        // Kiểm tra giá trị của trường birthday (Nếu bạn muốn kiểm tra)
        var birthday = $("input[type='date']").val();
        if (birthday === "") {
            $("#birthdayMessage").text("Hãy chọn ngày sinh");
            birthdayValid = false;
        } else {
            $("#birthdayMessage").text("");
        }

        // Ngăn chặn việc submit form nếu bất kỳ trường nào không hợp lệ
        if (!usernameValid || !genderValid || !departmentValid || !birthdayValid) {
            event.preventDefault();
        }
    });
});
