<html>
<head>
    <title>Sinh Viên</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="dashboard.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="register.js"></script>
</head>
<body>
<?php 
    // Thông tin kết nối MySQL
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "ltweb";

    // Tạo kết nối đến MySQL
    $conn = new mysqli($servername, $username, $password, $dbname);

    // Kiểm tra kết nối
    if ($conn->connect_error) {
        die("Kết nối thất bại: " . $conn->connect_error);
    }
    $NumStudentSql = "SELECT COUNT(*) AS count FROM students;";
    $studentSql = "SELECT `id`, `name`, `gender`, `department`, `birthday`, `address`, `image` FROM students;";
    $numStudent = $conn->query($NumStudentSql);
    $studentSql = $conn->query($studentSql);
    $numStudentRow = $numStudent->fetch_assoc();
?>
<form>
    
    <div class="search-container">
        <p class ='department-title'>Khoa<p> 
        <input type="text" class='department-search'>
    </div>
    <div class="search-container">
        <p class='keyword-title'>Từ khóa</p> 
        <input type="text" class='keyword-search'>
    </div>
    <button class='search-button'>Tìm kiếm</button>
</form>
<div class="container">
    <p class='num-student'> Số sinh viên tìm thấy: <?php echo $numStudentRow['count']?></p>
    <button class="add-students"><a href="register.php" class="add-students-link">Thêm</a></button>
</div>
<table>
    <tr>
        <td class='id'>No</td>
        <td class='name'>Tên Sinh Viên</td>
        <td class='department'>Khoa</td>
        <td class='action'>Action</td>
    </tr>
    <?php 
            while ($row = $studentSql->fetch_assoc()) {
                echo"<tr>";
                echo"<td class='id'>" . $row['id'] . "</td>";
                echo"<td class='name'>" . $row['name'] . "</td>";
                echo"<td class='department'>" . $row['department'] . "</td>";
                echo"<td class='action'>            
                    <button class='delete'><a href='#' class='delete' name='delete'>Xóa</a></button>
                    <button class='edit'><a href='#' class='edit' name='edit'>Sửa</a></button>
                </td>";
            }
    ?>
</table>