<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="register.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="register.js"></script>
</head>

<body>
    <div class="container">
        <form action='confirm.php' method="POST" enctype="multipart/form-data">
            <?php

            if ($_SERVER["REQUEST_METHOD"] == "POST" && !isset($_POST["confirm"])) {
                $username = $_POST["username"];
                $selectedGenders = $_POST["gender"];
                $birthday = $_POST["birthday"];
                $department = $_POST["department"];
                $address = $_POST["address"];
                echo "<input type='hidden' name='username' value='$username'>
                <input type='hidden' name='birthday' value='$birthday'>
                <input type='hidden' name='department' value='$department'>
                <input type='hidden' name='address' value='$address'>";
                if (isset($_FILES["image"]) && $_FILES["image"]["error"] == UPLOAD_ERR_OK) {
                    $tmp_name = $_FILES["image"]["tmp_name"];
                    $img_name = $_FILES["image"]["name"];
                    move_uploaded_file($tmp_name, "uploads/" . $img_name); // Di chuyển tệp đã tải lên vào một thư mục
                    $image_url = "uploads/" . $img_name; // URL đến hình ảnh đã tải lên
                    echo "<input type='hidden' name='image' value='$image_url'>";
                } else {
                    $image_url = "Không có hình ảnh được tải lên"; // Không có hình ảnh được tải lên
                }

                echo "<div class='input-container'>
                <div class='user-name' style='display: inline-block; vertical-align: middle;'>
                    <label for=user-name'><span class='text'>Họ và tên</span></label>
                </div>
                <div class='output-text' style='display: inline-block; vertical-align: middle;'>
                $username
                </div>
            </div>  ";

            if (is_array($selectedGenders)) {
                foreach ($selectedGenders as $selectedGender) {
                    if ($selectedGender == '0') {
                        
                        echo "
                        <input type='hidden' name='gender' value='$selectedGender'>
                        <div class='input-container'>
                            <div class='user-name' style='display: inline-block; vertical-align: middle;'>
                                <label><span class='text'>Giới tính</span></label>
                            </div>
                            <div class='output-text' style='display: inline-block; vertical-align: middle;'>Nam</div>
                        </div>";
                    }
                    if ($selectedGender == '1') {
                        echo "<input type='hidden' name='gender' value='$selectedGender'>
                        <div class='input-container'>
                            <div class='user-name' style='display: inline-block; vertical-align: middle;'>
                                <label><span class='text'>Giới tính</span></label>
                            </div>
                            <div class='output-text' style='display: inline-block; vertical-align: middle;'>Nữ</div>
                        </div>";
                    }
                }
            }
                if ($department == "KHVL") {
                    echo "            <div class='input-container'>
                <div class='user-name' style='display: inline-block; vertical-align: middle;'>
                <label><span class='text'>Phân khoa</span></label>
                </div>
                <div class='output-text' style='display: inline-block; vertical-align: middle;'>Khoa học vật lí</div>
            </div>";
                }
                if ($department == "KHMT") {
                    echo "            <div class='input-container'>
                <div class='user-name' style='display: inline-block; vertical-align: middle;'>
                <label><span class='text'>Phân khoa</span></label>
                </div>
                <div class='output-text' style='display: inline-block; vertical-align: middle;'>Khoa học máy tinh</div>
            </div>";
                }
                echo "            <div class='input-container'>
                <div class='user-name' style='display: inline-block; vertical-align: middle;'>
                <label><span class='text'>Ngày sinh</span></label>
            </div>
            <div class='output-text' style='display: inline-block; vertical-align: middle;'>$birthday</div>
            </div>";

                echo "<div class='input-container'>
            <div class='user-name' style='display: inline-block; vertical-align: middle;'>
                <label><span class='text'>Địa chỉ</span></label>
                </div>
                <div class='output-text' style='display: inline-block; vertical-align: middle;'>$address</div>
                </div>";
                if ($image_url != "Không có hình ảnh được tải lên") {
                    echo "<div class='input-container'>
                <div class='user-name' style='display: inline-block; vertical-align: middle;'>
                    <label>Hình ảnh</label>
                </div>
                <div class='output-text' style='display: inline-block; vertical-align: middle;'>
                <img src='$image_url' alt='Hình ảnh đã tải lên' width='200'>
                </div>
            </div>";
                }
                echo "</p>";


            echo "<div class='confirm-container'>
            <div class='confirm-button' style='display: inline-block; vertical-align: middle;'>
                <button style='background-color: rgb(115,173,73); color: white; border: none;'>
                    Xác nhận
                    <input type='hidden' name='confirm' value='true'>
                </button>
            </div>

            </div>
        </form>
    </div>";            
    }
            ?>
            <?php
            function urlToBlob($url)
            {
                $image_data = file_get_contents($url);
                if ($image_data === false) {
                    return false;
                }
                $image_blob = base64_encode($image_data);
                return $image_blob;
            }
            if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["confirm"])) {
                $gender = $_POST["gender"];
                $birthday = $_POST["birthday"];
                $department = $_POST["department"];
                $address = $_POST["address"];
                $image_url = $_POST["image"];
                $image_blob = urlToBlob($image_url);
                include('database.php'); // Bao gồm tệp kết nối cơ sở dữ liệu
                $insertGender = "";
                if ($gender == 0) {
                    $insertGender = "Nam";
                }
                if ($gender == 1) {
                    $insertGender = "Nữ";
                }
                $birthday = $_POST["birthday"];
                $insertDepartment = "";
                if ($department == "KHVL") {
                    $insertDepartment = "Khoa học vật lí";
                }
                if ($department == "KHMT") {
                    $insertDepartment = "Khoa học máy tính";
                }
                $address = $_POST["address"];
                $username = $_POST["username"];
                // Truy vấn SQL để chèn dữ liệu và đường dẫn hình ảnh
                $sql = "INSERT INTO `students` (`name`, `gender`, `birthday`, `department`, `address`, `image`) VALUES ('$username', '$insertGender', '$birthday', '$insertDepartment', '$address', '$image_blob')";

                if ($conn->query($sql) === TRUE) {
                    echo "Đã đăng kí thành công";
                } else {
                    echo "Lỗi: " . $sql . "<br>" . $conn->error;
                }

                $conn->close();
            }
            ?>
</body>

</html>