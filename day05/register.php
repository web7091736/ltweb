<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="register.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script> 
    <script src="register.js"></script>
</head>
<body>
    <div class="container">
        <p class='error' id="usernameMessage" style="color: red;"></p>
        <p class='error' id="genderMessage" style="color: red;"></p>
        <p class='error' id="departmentMessage" style="color: red;"></p>
        <p class='error' id="birthdayMessage" style="color: red;"></p>
        <form method="POST" id="registrationForm" action="confirm.php" enctype="multipart/form-data">

            <div class="input-container">
                <div class="user-name" style="display: inline-block; vertical-align: middle;">
                <label for="user-name"><span class="text">Họ và tên</span> <span class="required">*</span></label>
                </div>
                <div class="input-text" style="display: inline-block; vertical-align: middle;">
                    <input type="text" id="username" name="username" class="blue-rectangle" style="background-color: white; border: none; width: 98%; height: 90%;">
                </div>
            </div>    
            <div class="input-container">
                <div class="user-name" style="display: inline-block; vertical-align: middle;">
                <label><span class="text">Giới tính</span> <span class="required">*</span></label>
                </div>
                <div class="genderInput" style="display: inline-block; vertical-align: middle;">
                    <?php
                    $genders = array(0 => 'Nam', 1 => 'Nữ'); // Danh sách giới tính
                    for ($key = 0; $key < count($genders); $key++) {
                        echo '<label class="custom-checkbox">';
                        echo '<input class="" type="radio" id="gender' . $key . '" name="gender[]" value="' . $key . '">';
                        echo '<span class="checkmark"></span>'; // Thêm hình tròn
                        echo $genders[$key];
                        echo '</label>';
                    }
                    ?>
                </div>
            </div>

            <div class="input-container">
                <div class="user-name" style="display: inline-block; vertical-align: middle;">
                <label><span class="text">Phân khoa</span> <span class="required">*</span></label>
                </div>
                <div  style=" display: inline-block; vertical-align: middle;">
                    <select class="input" id="department" name="department">
                        <option value="PK">--Chọn phân khoa--</option>
                        <?php
                        $departments = array("KHMT" => "Khoa Học Máy Tính", "KHVL" => "Khoa Học Vật Lý"); // Danh sách các phân khoa
                        foreach ($departments as $key => $department) {
                            echo '<option id="department" name="department" value="' . $key . '">' . $department . '</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>

            
            <div class="input-container">
                <div class="user-name" style="display: inline-block; vertical-align: middle;">
                <label><span class="text">Ngày sinh</span> <span class="required">*</span></label>
                </div>  
                    <div  style=" display: inline-block; vertical-align: middle;">
                    <input type="date" name="birthday">
                </div>
            </div>
            <div class="input-container">
                <div class="user-name" style="display: inline-block; vertical-align: middle;">
                    <label>Địa chỉ</label>
                </div>
                <textarea name="address" class="input-textarea" rows="4" cols="50">
                </textarea>
                </div>
            <div class="input-container">
                <div class="user-name" style="display: inline-block; vertical-align: middle;">
                    <label>Hình ảnh</label>
                </div>
                    <input type="file" name="image">
                </div>
            </div>
            </div>

            <div class="submit-container">
                <div class="submit-button" style="display: inline-block; vertical-align: middle;">
                    <input type="submit" value="Đăng ký" style="background-color: rgb(115,173,73);color: white; border: none;">
                </div>
            </div>
        </form>
    </div>  
</body>
</html>

