<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="register.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script> 
    <script src="register.js"></script>
</head>
<body>
    <div class="container">
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $username = $_POST["username"];
        $gender = $_POST["gender"];
        $birthday = $_POST["birthday"];
        $department = $_POST["department"];
        $address = $_POST["address"];

        if (isset($_FILES["image"]) && $_FILES["image"]["error"] == UPLOAD_ERR_OK) {
            $tmp_name = $_FILES["image"]["tmp_name"];
            $img_name = $_FILES["image"]["name"];
            move_uploaded_file($tmp_name, "uploads/" . $img_name); // Move the uploaded file to a directory
            $image_url = "uploads/" . $img_name; // URL to the uploaded image
        } else {
            $image_url = "No image uploaded"; // No image uploaded
        }

        echo "<div class='input-container'>
            <div class='user-name' style='display: inline-block; vertical-align: middle;'>
                <label for=user-name'><span class='text'>Họ và tên</span></label>
            </div>
            <div class='output-text' style='display: inline-block; vertical-align: middle;'>
            $username
            </div>
        </div>  ";

        if (is_array($gender)) {
                if(implode(", ", $gender)=='0'){
                    echo "<div class='input-container'>
                    <div class='user-name' style='display: inline-block; vertical-align: middle;'>
                    <label><span class='text'>Giới tính</span></label>
                    </div>
                    <div class='output-text' style='display: inline-block; vertical-align: middle;'>Nam</div>
                </div>";
                }
                if(implode(", ", $gender)=='1'){
                    echo "<div class='input-container'>
                    <div class='user-name' style='display: inline-block; vertical-align: middle;'>
                    <label><span class='text'>Giới tính</span></label>
                    </div>
                    <div class='output-text' style='display: inline-block; vertical-align: middle;'>Nữ </div>
                </div>";
                }
        }
        if($department=="KHVL"){
            echo "            <div class='input-container'>
            <div class='user-name' style='display: inline-block; vertical-align: middle;'>
            <label><span class='text'>Phân khoa</span></label>
            </div>
            <div class='output-text' style='display: inline-block; vertical-align: middle;'>Khoa học vật lí</div>
        </div>";
        }
        if($department=="KHMT"){
            echo "            <div class='input-container'>
            <div class='user-name' style='display: inline-block; vertical-align: middle;'>
            <label><span class='text'>Phân khoa</span></label>
            </div>
            <div class='output-text' style='display: inline-block; vertical-align: middle;'>Khoa học máy tinh</div>
        </div>";
        }
        echo "            <div class='input-container'>
        <div class='user-name' style='display: inline-block; vertical-align: middle;'>
        <label><span class='text'>Ngày sinh</span></label>
    </div>
    <div class='output-text' style='display: inline-block; vertical-align: middle;'>$birthday</div>
    </div>";

        echo "<div class='input-container'>
        <div class='user-name' style='display: inline-block; vertical-align: middle;'>
            <label>Địa chỉ</label>
            <div class='output-text' style='display: inline-block; vertical-align: middle;'>$address</div>
        </div>
    </div>";
        if ($image_url != "No image uploaded") {
            echo "<div class='input-container'>
            <div class='user-name' style='display: inline-block; vertical-align: middle;'>
                <label>Hình ảnh</label>
            </div>
            <div class='output-text' style='display: inline-block; vertical-align: middle;'>
            <img src='$image_url' alt='Uploaded Image' width='200'>
            </div>
        </div>";
        } else {
            echo "No image uploaded";
        }
        echo "</p>";
    }
    ?>
                <div class="confirm-container">
                <div class="confirm-button" style="display: inline-block; vertical-align: middle;">
                    <button style="background-color: rgb(115,173,73);color: white; border: none;">Xác nhận</button>
                </div>
            </div>
    </div>
</body>
</html>
