<html>

<head>
    <title>Sinh Viên</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="dashboard.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="register.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script>
        function searchStudents() {
            // Lấy giá trị của phần tử có id là 'department'
            var department = $('#department').val();
            var keyword = $('#searchInput').val();

            // Kiểm tra nếu có ít nhất một trong hai giá trị department hoặc keyword tồn tại
            if (department || keyword) {
                // Sử dụng AJAX để gửi yêu cầu POST đến URL 'dashboard.php'
                $.ajax({
                    type: 'POST', // Phương thức yêu cầu là POST
                    url: 'dashboard.php', // Đường dẫn tới tệp xử lý yêu cầu
                    data: { department, keyword }, //them gia tri vao data gui di
                    success: function(data) {
                        // Khi yêu cầu thành công, chạy hàm
                        // removeUnwantedForm để xử lý dữ liệu trả về
                        data = removeUnwantedForm(data);
                        // Thêm dữ liệu đã xử lý vào phần tử có id là 'studentTable'
                        $('#studentTable').html(data);
                    }
                });
            }
        }

        function resetForm() {
            // Sử dụng jQuery để lấy phần tử form có id là 'search-form'
            var form = $("#search-form")[0];

            // Sử dụng jQuery để thiết lập selectedIndex của phần tử 'department' trong form về 0
            $("select[name='department']", form).prop('selectedIndex', 0);

            // Sử dụng jQuery để thiết lập giá trị của phần tử 'search' trong form về chuỗi rỗng
            $("input[name='search']", form).val('');
        }


        function removeUnwantedForm(data) {
            var $temp = $('<div>').html(data);
            $temp.find('#search-form').remove();
            $temp.find('.search-button').remove();
            return $temp.html();
        }
        $deleteModal = false;
        function openModal(studentId) {
            $('#deleteModal').modal('show');
        }

        function closeModal() {
            $('#deleteModal').modal('hide');
        }

        function deleteInfo(studentId) {
        // Thiết lập ID của sinh viên cần xóa trong trường ẩn
            $('#idToDelete').val(studentId);
            $deleteModal = true;
            openModal(studentId); // Mở modal trước khi xóa
}

    </script>
</head>

<body>
    <?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "ltweb";

    // Tạo kết nối đến MySQL
    $conn = new mysqli($servername, $username, $password, $dbname);

    // Kiểm tra kết nối
    if ($conn->connect_error) {
        die("Kết nối thất bại: " . $conn->connect_error);
    }
    $NumStudentSql = "SELECT COUNT(*) AS count FROM students;";
    $numStudent = $conn->query($NumStudentSql);
    $numStudentRow = $numStudent->fetch_assoc();
    if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["delete"])) {
        $idToDelete = $_POST["idToDelete"];
        $conn = new mysqli($servername, $username, $password, $dbname);
        $id = $_POST["id"];
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
         $sql = "DELETE FROM students WHERE id = '$idToDelete' ";
        if ($conn->query($sql) === TRUE) {
            header("Location: dashboard.php");
            exit();
        } else {
            echo "Error deleting news article: " . $conn->error;
        }
        // Close the database connection
        $conn->close();
        
    }
    ?>

    <!-- Modal -->
    <div class="modal" tabindex="-1" role="dialog" id="deleteModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Xóa</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p>Bạn muốn xóa sinh viên này?</p>
        </div>
        <div class="modal-footer">
            <form action="dashboard.php" method="post" id='deleteForm'>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button> 
                <input type='hidden' name='idToDelete' id='idToDelete'>
                <button type="submit" name='delete' class="btn btn-danger">Xóa</button>
            </form>
        </div>
        </div>
    </div>
    </div>
    <!-- Search Form -->
    <form id="search-form" action="dashboard.php" method="post">
        <div class="search-container">
            <div class='department-title'>
                <p>Khoa</p>
            </div>
            <select name="department" id="department" onchange="searchStudents()">
                <option>--Chọn Khoa--</option>
                <option value="KHMT">Khoa học máy tính</option>
                <option value="KHVL">Khoa học vật lý</option>
            </select>
        </div>
        <div class="search-container">
            <div class='keyword-title'>
                <p>Từ khóa</p>
            </div>
            <input name="search" type="text" class='keyword-search' id='searchInput' onkeyup="searchStudents()">
        </div>
    </form>
    <button onclick="resetForm()" name="reset-button" class='search-button'>Reset</button>
    <div id="studentTable">
        <div class="container">
            <p class='num-student'> Số sinh viên tìm thấy: <?php echo $numStudentRow['count'] ?></p>
            <button class="add-students"><a href="register.php" class="add-students-link">Thêm</a></button>
        </div>
        <table>
            <tr>
                <td class='id'>No</td>
                <td class='name'>Tên Sinh Viên</td>
                <td class='department'>Khoa</td>
                <td class='action'>Action</td>
            </tr>

            <?php

            $tableContent = '';
            $searchSql = "";
            if ($_SERVER["REQUEST_METHOD"] == "POST") {

                $department = $_POST['department'];
                $keyword = $_POST['keyword'];
                $selectedDepartment = $_POST["department"];
                if ($selectedDepartment == "--Chọn Khoa--") {
                    $selectedDepartment = "";
                } elseif ($selectedDepartment == "KHMT") {
                    $selectedDepartment = "Khoa học máy tính";
                } elseif ($selectedDepartment == "KHVL") {
                    $selectedDepartment = "Khoa học vật lý";
                }
                if ($selectedDepartment) {
                     $searchSql = " SELECT * FROM `students` WHERE `department` LIKE '$selectedDepartment' AND (`id` LIKE '%$keyword%' OR `name` LIKE '%$keyword%')";
                } else {
                     $searchSql = " SELECT * FROM `students` WHERE `department` LIKE '%$keyword%' OR `id` LIKE '%$keyword%' OR `name` LIKE '%$keyword%'";
                };
            } else {
                $searchSql = "SELECT * FROM `students`";
            }
            $studentSql = $conn->query($searchSql);
            while ($row = $studentSql->fetch_assoc()) {
                $id = $row['id'];
                echo "<tr>";
                echo "<td class='id'>" . $row['id'] . "</td>";
                echo "<td class='name'>" . $row['name'] . "</td>";
                echo "<td class='department'>" . $row['department'] . "</td>";
                echo "<td class='action'>     
                <form action='dashboard.php' method='POST'>       
                <input type='hidden' name='id' value='" . $row["id"] . "'>
                <button class='delete' type='button' onclick='deleteInfo(" . $row["id"] . ")'> Xóa </button>
                </form>
                <form action='update_students.php' method='post'>
                <input type='hidden' name='id' value='" . $row["id"] . "'>
                <input type='hidden' name = 'name' value= '" . $row["name"] . "'>
                <input type='hidden' name = 'gender' value= '" . $row["gender"] . "'>
                <input type='hidden' name = 'department' value= '" . $row["department"] . "'>
                <input type='hidden' name = 'birthday' value= '" . $row["birthday"] . "'>
                <input type='hidden' name = 'address' value= '" . $row["address"] . "'>
                <input type='hidden' name = 'image' value= '" . $row["image"] . "'>
                <button class='edit' type='submit' name='edit'>Sửa</button>
                </form>
            </td>";
            }
            ?>
        </table>
    </div>

</body>

</html>