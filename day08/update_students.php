<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="register.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="register.js"></script>
</head>

<body>
    <div class="container">
        <form action='update_students.php' method="POST" enctype="multipart/form-data">
            <?php

            if ($_SERVER["REQUEST_METHOD"] == "POST" && !isset($_POST["confirm"]) && !isset($_POST["confirmEdit"]) ) {
                $id = $_POST["id"];
                $name = $_POST["name"];
                $gender = $_POST["gender"];
                $birthday = $_POST["birthday"];
                $department = $_POST["department"];
                $address = $_POST["address"];
                echo "<input type='hidden' value='$id' id='id' name='id'>";
                $blobData = $_POST["image"];

                $imageData = base64_decode($blobData);

                // Specify the path where you want to save the image
                $imagePath = 'output.png';
                
                // Save the image data to a file
                file_put_contents($imagePath, $imageData);
                
                echo "<div class='input-container'>
                <div class='user-name' style='display: inline-block; vertical-align: middle;'>
                    <label for=user-name'><span class='text'>Họ và tên</span></label>
                </div>
                <div class='input-text' style='display: inline-block; vertical-align: middle;'>
                <input type='text' value='$name' id='username' name='name' class='blue-rectangle' style='background-color: white; border: none; width: 98%; height: 90%;'>
            </div>
            </div>  ";

            echo "<div class='input-container'>
                <div class='user-name' style='display: inline-block; vertical-align: middle;'>
                    <label for=user-name'><span class='text'>Giới tính</span></label>
                </div>
                <div class='genderInput' style='display: inline-block; vertical-align: middle;'>";
                    
                $genders = array(0 => 'Nam', 1 => 'Nữ'); // Danh sách giới tính
                
                foreach ($genders as $genderValue => $genderLabel) {
                    echo '<label class="custom-checkbox">';
                    echo '<input class="" type="radio" id="gender' . $genderValue . '" name="gender[]" value="' . $genderValue . '"';
                    // Kiểm tra nếu giá trị hiện tại là giá trị mặc định
                    if ($gender == $genderLabel) {
                        echo ' checked'; // Đánh dấu là đã chọn nếu trùng khớp
                    }
                    echo '>';
                    echo '<span class="checkmark"></span>';
                    echo $genderLabel;
                    echo '</label>';
                }
                

            echo"    </div>
            </div>  ";
            echo "<div class='input-container'>
            <div class='user-name' style='display: inline-block; vertical-align: middle;'>
                <label for=user-name'><span class='text'>Phân khoa</span></label>
            </div>
            <div  style=' display: inline-block; vertical-align: middle;'>
            <select class='input' id='department' name='department'>
                ";      
                $departments = array("KHMT" => "Khoa học máy tính", "KHVL" => "Khoa học vật lý"); // Danh sách các phân khoa
                foreach ($departments as $departmentValue => $departmentLabel) {
                    echo '<option id="department" name="department" value="' . $departmentValue . '"';
                    if ($department == $departmentLabel) {
                        echo ' selected';
                    }
                    echo "> $departmentLabel </option>";
                }
                  
                    
            echo "</select>
        </div>
        </div>  ";
                echo "<div class='input-container'>
                <div class='user-name' style='display: inline-block; vertical-align: middle;'>
                <label><span class='text'>Ngày sinh</span></label>
            </div>
            <div  style=' display: inline-block; vertical-align: middle;'>
                    <input type='date' name='birthday' value='$birthday'>
                    </div>
            </div>";

                echo "<div class='input-container'>
            <div class='user-name' style='display: inline-block; vertical-align: middle;'>
                <label><span class='text'>Địa chỉ</span></label>
                </div>
                <div class='input-text' style='display: inline-block; vertical-align: middle;'>
                <input type='text' value='$address' id='username' name='address' class='blue-rectangle' style='background-color: white; border: none; width: 98%; height: 90%;'>
            </div>
                </div>";
                    echo "<div class='input-container'>
                <div class='user-name' style='display: inline-block; vertical-align: middle;'>
                    <label>Hình ảnh</label>
                </div>
                <div class='output-text' style='display: inline-block; vertical-align: middle;'>
                    <div style='display: flex;'>
                        <img src='$imagePath' alt='Hình ảnh đã tải lên' width='200'>
                    </div>
                </div>
            </div>";
                echo "</p>";

            echo "<div class='confirm-container'>
            <div class='confirm-button' style='display: inline-block; vertical-align: middle;'>
                <button style='background-color: rgb(115,173,73); color: white; border: none;'>
                    Xác nhận
                    <input type='hidden' name='confirmEdit' value='true'>
                </button>
            </div>

            </div>
        </form>
    </div>";            
    }
            ?>
            <?php
            function urlToBlob($url)
            {
                $image_data = file_get_contents($url);
                if ($image_data === false) {
                    return false;
                }
                $image_blob = base64_encode($image_data);
                return $image_blob;
            }

            if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["confirmEdit"])) {
                
                $gender = $_POST["gender"];
                $id = $_POST["id"];
                $birthday = $_POST["birthday"];
                $department = $_POST["department"];
                $address = $_POST["address"];
                // $image_url = $_POST["image"];
                // $image_blob = urlToBlob($image_url);
                $insertGender = ($gender == 0) ? "Nam" : "Nữ";
                $name = $_POST["name"];
                $insertDepartment = ($department == "KHVL") ? "Khoa học vật lí" : "Khoa học máy tính";
                include('database.php'); // Bao gồm tệp kết nối cơ sở dữ liệu
                // Sử dụng prepared statement để tránh SQL injection
                $stmt = $conn->prepare("UPDATE students SET name = ?, gender = ?, birthday = ?, department = ?, address = ? WHERE id = ?");
                $stmt->bind_param("ssssss",$name, $insertGender, $birthday, $insertDepartment, $address, $id);

                if ($stmt->execute()) {
                    header("Location: dashboard.php");
                    exit(); // Đảm bảo dừng việc thực thi mã nguồn ngay sau hàm header
                } else {
                    echo "Lỗi: " . $stmt->error;
                }

                $stmt->close();
                $conn->close();
            }
            ?>

</body>

</html>